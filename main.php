<?php

include_once __DIR__.'/master.php';

// debug(var_export($s,true));

// warning非表示
error_reporting(1);

$filename = $argv[1];

// パラメータチェックする
if(empty($filename)){
	debug('パラメータなし');
	exit;
}
// ファイルが存在チェックする
if (($handle = fopen($filename, "r")) !== FALSE) {
    // 1行ずつ読み込む
    while (($data = fgetcsv($handle))) {
        // debug(var_export($data, true));

        $result = check_shikuchouson($data[1]);
        if(!empty($result)){
            // debug($data[0]);
            // debug(var_export($result, true));
            foreach ($result as $key => $value) {
                if(empty($value)){
                    debug($data[0].','.$key);
                }
                foreach ($value as $key2 => $value2) {
                    if(empty($value2)){
                        debug($data[0].','.$key.','.$key2);
                    }else{
                        foreach ($value2 as $key3 => $value3) {
                            if(!empty($value3)){
                                debug($data[0].','.$key.','.$key2.','.$value3);
                            }else{
                                debug($data[0].','.$key.','.$key2);
                            }
                        }                        
                    }                    
                }
            }
        }
    }
    fclose($handle);
}else{
	debug('ファイルが存在しない');
}

function debug($text){
	echo $text.PHP_EOL;
}

// 市区町村チェック
function check_shikuchouson($data) {
    global $master_city; // 市区町村マスタ
    debug($data);

    $result = array();

    foreach ($master_city as $v) {
        $result_child = array();

        $shikuchouson = $v[1];
        if(empty($shikuchouson)){
            continue;
        }
        // 市区町村名が含まれるか？
        if(strpos($data,$shikuchouson) !== false){
            // debug($shikuchouson);
            

            $detail = check_detail($data, $shikuchouson);
            if(!empty($detail)){
                $result[$v[0]][$shikuchouson] = $detail;
            }else{
                $result[$v[0]][$shikuchouson] = array();
            }

        // 都道府県が含まれるか？
        }else if(strpos($data,$v[0]) !== false){
            if(!array_key_exists($v[0],$result)){
                $result[$v[0]] = array();
            }
        }
    }

    return $result;
}

// 詳細チェック
function check_detail($data, $shikuchouson){
    $a = $data;

    $result = array();
    // debug($shikuchouson);
    $count = 0;
    while (strpos($a,$shikuchouson)){
        // debug("a=".$a);
        // 市区町村出現位置
        $start = strpos($a,$shikuchouson);
        // debug("start=".$start);
        // 市区町村出現位置で切り取り
        $str = substr($a, $start);
        // 最初の市区町村を削除
        $str2 = preg_replace("/".$shikuchouson."/", "", $str, 1);

        // debug("str2=".$str2);

        $a = $str2;        

        // 数値出現まで分割
        $parts = preg_split(
            '/^([^\d０-９－―-]*+[\d０-９－―-]*+)/u',
            $str2,
            2,
            PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
        );
        // debug(var_export($parts[0], true));

        // 多分番地の出現位置
        $start2 = strpos($a,$parts[0]);

        // debug($start2 - $start);
        $diff = $start2 - $start;

        if(!strpos($parts[0],'・') >= 1 && !strpos($parts[0],' ')){
            if($diff <= 30){
                $result[] = $parts[0];
            }            
        }

    }
    // debug(var_export($result, true));

    return $result;
}